/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-login-signin.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-login-signin></sellfone-login-signin>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});

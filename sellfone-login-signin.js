import { html, LitElement } from 'lit-element';
import style from './sellfone-login-signin-styles.js';

class SellfoneLoginSignin extends LitElement {
  static get properties() {
    return {
       userLogin:{
         type:Object,
         attribute:"user-login"
       },
       userRegister:{
         type:Object,
         attribute:"user-register"
       },
       visibleRegistry:{
          type:Boolean,
          attribute:"visible-registry"
       },
       _errors:{
         type:Boolean,
         attribute:"errors"
       }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.userLogin={};
    this.userRegister={};
    this.visibleRegistry=false;
    this._errors=false;
  }

  render() {
    return html`
        <div class="main-component">
          ${this.visibleRegistry? html`
          ${this._createRegistryTemplate()}
          `:html`
            ${this._createLoginTemplate()}
          `}
             
        </div>
      `;
    }

    _createRegistryTemplate(){
      return html`
      <div class="sub-container">
                  <div class="container">
                        <h2>Crear una cuenta</h2>
                        <label for="email">Email:<span class="mandatory">*</span></label>
                        <input class="login-input" id="email" type="text" placeholder="">
                        <label for="passwordRegister">Contraseña<span class="mandatory">*</span></label>
                        <input class="login-input" id="passwordRegister" type="password" placeholder="">
                          ${this._errors? html` <p class="errors">*Verificar que los campos esten llenados correctamente</p>`:html``}
                        <div class="sub-link-container">
                        
                          <div class="linkContainer">
                          <button class="mainButton" @click="${(e)=>this.makeVisibleRegistry(e)}">Iniciar Sesión</button>
                          </div>
                          <div class="linkContainer">
                          <button class="mainButton registerButton" @click="${(e)=>this.registerUser(e)}">Crear una cuenta</button>
                          </div>
                          
                        </div>
                        
                  </div>
                  
              </div>

      `
    }


    _createLoginTemplate(){
      return html`
      <div class="sub-container">
                  <div class="container">
                        <h2>Iniciar Sesión</h2>
                        <label for="userName">Nombre de usuario o email <span class="mandatory">*</span></label>
                        <input class="login-input" id="userName" type="text" placeholder="">
                        <label for="userName">Contraseña <span class="mandatory">*</span></label>
                        <input class="login-input" id="password" type="password" placeholder="">
                          ${this._errors? html` <p class="errors">* Verificar que los campos esten llenados correctamente</p>`:html``}
                        
                        <div class="sub-link-container">
                          <div class="linkContainer">
                          <input type="checkbox" id="rememberPassword">
                          <label for="rememberPassword">Recordarme</label>
                          </div>
                          <div class="linkContainer">
                          <a href="#" class="link">Olvidaste tu contraseña?</a>
                          </div>
                        </div>
                        <div class="sub-link-container">
                        <div class="linkContainer">
                          <button class="mainButton registerButton" @click="${(e)=>this.makeVisibleRegistry(e)}">Crear una cuenta</button>
                          </div>
                          <div class="linkContainer">
                          <button class="mainButton" @click="${(e)=>this.login(e)}">Iniciar Sesión</button>
                          </div>
                          
                        </div>
                        
                  </div>
                  
              </div>

      `
    }

    login(event){
      this.userLogin={
        userName:this.shadowRoot.querySelector("#userName").value,
        userPassword:this.shadowRoot.querySelector("#password").value,
        rememberPassword:this.shadowRoot.querySelector("#rememberPassword").checked
      };
      if(this.userLogin.userName.length>0 && this.userLogin.userPassword.length>0){
        this.dispatchEvent(new CustomEvent("user-login",{
          bubbles:false,
          composed:false,
          detail:this.userLogin
        }));
      }else{
        this._errors=true;
        this.dispatchEvent(new CustomEvent("user-login-error",{
          bubbles:false,
          composed:false,
          detail:{user:this.userLogin,error:"fields not filled properly"}
        }));
      }
     
    }

    makeVisibleRegistry(){
      this.visibleRegistry=!this.visibleRegistry;
    }

    _validateEmail(){
      const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const email=this.shadowRoot.querySelector("#email").value;
      return regex.test(String(email).toLowerCase());
    }

    registerUser(){
      this.userRegister={
        userName:this.shadowRoot.querySelector("#email").value,
        userPassword:this.shadowRoot.querySelector("#passwordRegister").value,
      };

      if(this._validateEmail()&&this.userRegister.userPassword.length>0){
        this.dispatchEvent(new CustomEvent("user-registry",{
          bubbles:false,
          composed:false,
          detail:this.userRegister
        }));
      }else{
        this._errors=true;
        this.dispatchEvent(new CustomEvent("user-registry-error",{
          bubbles:false,
          composed:false,
          detail:{user:this.userRegister,error:"fields not filled properly"}
        }));
      }

    }

    



}

window.customElements.define("sellfone-login-signin", SellfoneLoginSignin);

![sellfone-login-signin screenshot](sellfone-login-signin.svg)
# <sellfone-login-signin>

![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)
![Travis CI](https://travis-ci.org/github_username/sellfone-login-signin.svg?branch=master)

> This is an example file with default selections.

## Install

```html
    <script type="module">
        import '@catsys/sellfone-login-signin.js';
    </script>
```

## Usage

- You can use `yarn` or `npm install` for install the dependencies and devDependencies
- `catsys component:serve` - Developing server
- `catsys component:test` - Run unit testing

## Contributing

PRs accepted. Please, contributing

## License

MIT © Alfonso Ríos
